/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "@fontsource/roboto";

import '@fortawesome/fontawesome-free';

import './styles/app.scss';

// specify which plugins you need:
import { Tooltip, Toast, Popover } from 'bootstrap'

import './js/mdb.free';

// start the Stimulus application
import './bootstrap';